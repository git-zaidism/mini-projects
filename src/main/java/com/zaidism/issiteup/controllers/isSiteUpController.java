package com.zaidism.issiteup.controllers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.zaidism.issiteup.constants.Constants;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class isSiteUpController {

    @GetMapping("/check")
    public String getUrlStatusMessage(@RequestParam String url) {
        String returnMessage = "";
        try {
            URL urlObj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            int responseCodeRecieved = connection.getResponseCode() / 100;

            if (responseCodeRecieved == 2 || responseCodeRecieved == 3 || responseCodeRecieved == 1) {
                returnMessage = Constants.SITE_UP;
            } else {
                returnMessage = Constants.SITE_DOWN;
            }

        } catch (MalformedURLException e) {
            returnMessage = Constants.URL_INCORRECT;
        } catch (IOException e) {
            returnMessage = Constants.SITE_DOWN;
        }

        return returnMessage;

    }

}
